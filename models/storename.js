const Joi = require('joi');
const Schema = require('../config/schema');

const tableName = `Store_details`;
const storeSchema = {
	hashKey: 'storeName',
	// rangeKey:'username',
	timestamps: true,
	schema: Joi.object({
		storeName: Joi.string(),
        merchant_id:Joi.string()
	})
};
const optionsObj = {};
const Storename = Schema(storeSchema, {tableName:tableName});

module.exports = Storename;
